"use strict";

const numberOfFilms = +prompt("Сколько фильмов вы уже посмотрели?");

const personalMovieDB = {
  count: numberOfFilms,
  movies: {},
  actors: {},
  genres: [],
  privat: false
}

/*
let answersGiven = 0;

while (answersGiven < numberOfFilms) {
  let lastViewedMovie = prompt("Один из последних просмотренных фильмов?");
  if (lastViewedMovie === "" || lastViewedMovie.length > 50) {
    alert("Повторите ввод");
    continue;
  }
  let score = +prompt("На сколько оцените его?");
  personalMovieDB.movies[lastViewedMovie] = score;
  answersGiven++;
}
*/

/*
for (let answersGiven = 0; answersGiven < numberOfFilms; answersGiven++) {
  let lastViewedMovie = prompt("Один из последних просмотренных фильмов?");
  if (lastViewedMovie === "" || lastViewedMovie.length > 50) {
    alert("Повторите ввод");
    answersGiven--;
    continue;
  }
  let score = +prompt("На сколько оцените его?");
  personalMovieDB.movies[lastViewedMovie] = score;
}
*/

let answersGiven = 0;

do {
  let lastViewedMovie = prompt("Один из последних просмотренных фильмов?");
  if (lastViewedMovie === "" || lastViewedMovie.length > 50) {
    alert("Повторите ввод");
    continue;
  }
  let score = +prompt("На сколько оцените его?");
  personalMovieDB.movies[lastViewedMovie] = score;
  answersGiven++;
} while (answersGiven < numberOfFilms);

if (personalMovieDB.count < 10) {
  alert("Просмотрено довольно мало фильмов");
} else if (10 <= personalMovieDB.count && personalMovieDB.count <= 30) {
  alert("Вы классический зритель");
} else if (personalMovieDB.count > 30) {
  alert("Вы киноман");
} else {
  alert("Произошла ошибка");  // Никогда не выполнится, т.к. count это число
}

console.log(personalMovieDB);
